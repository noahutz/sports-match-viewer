/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.repository.request

import com.google.gson.annotations.SerializedName

data class Match(
    @SerializedName("match_id") val matchId: String,
    @SerializedName("team_A") val teamA: Team,
    @SerializedName("team_B") val teamB: Team,
    @SerializedName("stat_type") val statType: String
)

data class Team(
    val id: Int,
    val name: String,
    val code: String,
    @SerializedName("short_name") val shortName: String,
    @SerializedName("top_players") val topPlayers: List<Player>
)

data class Player(
    val id: Int,
    val position: String,
    @SerializedName("full_name") val fullName: String,
    @SerializedName("short_name") val shortName: String,
    @SerializedName("stat_value") val statValue: Int,
    @SerializedName("jumper_number") val jumperNumber: Int
)