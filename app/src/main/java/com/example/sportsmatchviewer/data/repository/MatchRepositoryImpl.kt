/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.repository

import com.example.sportsmatchviewer.data.repository.request.Match
import com.example.sportsmatchviewer.data.repository.request.MatchRequest
import com.example.sportsmatchviewer.data.repository.request.mapper.MatchDataMapper
import com.google.gson.Gson

object MatchRepositoryImpl : MatchRepository {

    override fun getMatchList() =
        with(MatchRequest.getData()) {
            val matchList = Gson().fromJson(this, Array<Match>::class.java).toList()
            MatchDataMapper.map(matchList)
        }
}