/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.repository.request.mapper

import com.example.sportsmatchviewer.data.repository.request.Match
import com.example.sportsmatchviewer.data.repository.request.Player
import com.example.sportsmatchviewer.data.repository.request.Team
import com.example.sportsmatchviewer.data.Match as ModelMatch
import com.example.sportsmatchviewer.data.Team as ModelTeam
import com.example.sportsmatchviewer.data.Player as ModelPlayer


object MatchDataMapper {

    fun map(matchList: List<Match>) =
        matchList.map {
            ModelMatch(it.matchId, mapTeam(it.teamA), mapTeam(it.teamB))
        }

    private fun mapTeam(team: Team) =
        ModelTeam(team.id, team.name, team.code, team.shortName, mapPlayers(team.topPlayers))

    private fun mapPlayers(listPlayer: List<Player>) =
        listPlayer.map {
            ModelPlayer(
                it.id,
                it.position,
                it.fullName,
                it.shortName,
                it.statValue,
                it.jumperNumber
            )
        }
}