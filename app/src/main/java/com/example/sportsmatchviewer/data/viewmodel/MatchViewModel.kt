/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sportsmatchviewer.data.Match
import com.example.sportsmatchviewer.data.repository.MatchRepository
import java.lang.Exception
import java.net.UnknownHostException
import java.util.concurrent.Executors

class MatchViewModel(private val repository: MatchRepository) : ViewModel() {

    val matchList: MutableLiveData<List<Match>> = MutableLiveData()

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()

    fun loadData() {
        Executors.newScheduledThreadPool(1).execute {
            isLoading.postValue(true)
            try {
                val data = repository.getMatchList()
                matchList.postValue(data)
            } catch (e: UnknownHostException) {
                Log.e(TAG, "Exception: $e")
                matchList.postValue(emptyList())
            }
            isLoading.postValue(false)
        }
    }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}