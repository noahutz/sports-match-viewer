/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.repository

import com.example.sportsmatchviewer.data.Match

interface MatchRepository {

    fun getMatchList() : List<Match>
}