/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sportsmatchviewer.data.repository.MatchRepository

class MatchViewModelFactory(private val repository: MatchRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MatchViewModel::class.java)) {
            return modelClass.cast(MatchViewModel(repository)) as T
        }
        throw IllegalArgumentException("Unable to create ViewModelProvider")
    }
}