/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data

import java.io.Serializable

data class Match(
    val matchId: String,
    val teamA: Team,
    val teamB: Team
) : Serializable {
    val matchTitle = "${teamA.fullName} vs ${teamB.fullName}"
}

data class Team(
    val id: Int,
    val name: String,
    val code: String,
    val shortName: String,
    val topPlayers: List<Player>
) : Serializable {
    val fullName = "$name $shortName"
}

data class Player(
    val id: Int,
    private val position: String,
    val fullName: String,
    val shortName: String,
    private val statValue: Int,
    private val jumperNumber: Int
) : Serializable {
    fun getImageUrl() =
        "http://media.foxsports.com.au/match-centre/includes/images/headshots/nrl/$id.jpg"

    fun getPosition() = "Position: $position"
    fun getStatValue() = "Stat value: $statValue"
    fun getJumperNumber() = "Number: $jumperNumber"
}