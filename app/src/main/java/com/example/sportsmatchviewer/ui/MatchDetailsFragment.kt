/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sportsmatchviewer.R
import com.example.sportsmatchviewer.data.Match
import com.example.sportsmatchviewer.data.Team
import com.example.sportsmatchviewer.ui.adapter.PlayerListAdapter
import kotlinx.android.synthetic.main.fragment_match_details.*


class MatchDetailsFragment private constructor() : Fragment() {

    private var matchDetails: Match? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        matchDetails = arguments?.getSerializable(ARG_MATCH_DETAILS) as Match?
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_match_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        matchDetails?.apply {
            setDetailsMatch(this)
        }

        if (matchDetails == null) {
            textViewError.visibility = View.VISIBLE
        }
    }

    private fun setDetailsMatch(match: Match) {
        textViewTeamAName.text = match.teamA.fullName
        setDetailsTeam(match.teamA, recyclerViewTeamA)

        textViewTeamBName.text = match.teamB.fullName
        setDetailsTeam(match.teamB, recyclerViewTeamB)
    }

    private fun setDetailsTeam(
        team: Team,
        recyclerView: RecyclerView
    ) {
        recyclerView.adapter = PlayerListAdapter(team.topPlayers)
    }

    companion object {
        private const val ARG_MATCH_DETAILS = "Match Details"

        fun newInstance(matchDetails: Match): MatchDetailsFragment {
            val fragment = MatchDetailsFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(ARG_MATCH_DETAILS, matchDetails)
            }
            return fragment
        }
    }
}
