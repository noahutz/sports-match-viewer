/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.sportsmatchviewer.R
import com.example.sportsmatchviewer.data.repository.MatchRepositoryImpl
import com.example.sportsmatchviewer.data.viewmodel.MatchViewModel
import com.example.sportsmatchviewer.data.viewmodel.MatchViewModelFactory
import com.example.sportsmatchviewer.ui.adapter.MatchListAdapter
import kotlinx.android.synthetic.main.fragment_match_list.*

class MatchListFragment : Fragment() {

    private lateinit var adapter: MatchListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_match_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MatchListAdapter(fragmentManager as FragmentManager).apply {
            viewPager.adapter = this
        }

        val activity = activity as MainActivity
        val viewModelFactory = MatchViewModelFactory(MatchRepositoryImpl)
        ViewModelProviders.of(this, viewModelFactory).get(MatchViewModel::class.java).apply {
            isLoading.observe(activity, Observer {
                progressBar.visibility = if (it) View.VISIBLE else View.GONE
            })

            matchList.observe(activity, Observer {
                buttonRetry.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
                adapter.setMatchList(it)
            })

            buttonRetry.setOnClickListener {
                loadData()
            }

            loadData()
        }
    }
}
