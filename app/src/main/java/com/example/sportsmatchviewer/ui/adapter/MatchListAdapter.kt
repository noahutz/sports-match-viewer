/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.ui.adapter

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sportsmatchviewer.data.Match
import com.example.sportsmatchviewer.ui.MatchDetailsFragment


class MatchListAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var matchList = listOf<Match>()

    fun setMatchList(matchList: List<Match>) {
        this.matchList = matchList
        notifyDataSetChanged()
    }

    override fun getItem(position: Int) =
        MatchDetailsFragment.newInstance(matchList[position])

    override fun getCount() = matchList.size

    override fun getPageTitle(position: Int) = matchList[position].matchTitle
}