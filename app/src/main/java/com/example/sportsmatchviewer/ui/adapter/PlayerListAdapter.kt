/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sportsmatchviewer.R
import com.example.sportsmatchviewer.data.Player
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_player_item.view.*

class PlayerListAdapter(private val playerList: List<Player>) :
    RecyclerView.Adapter<PlayerListAdapter.PlayerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PlayerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_player_item,
                parent,
                false
            )
        )

    override fun getItemCount() = playerList.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) =
        holder.bind(playerList[position])

    class PlayerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(player: Player) {
            itemView.apply {
                Picasso.get()
                    .load(player.getImageUrl())
                    .placeholder(R.drawable.ic_player_headshot)
                    .error(R.drawable.ic_player_headshot)
                    .into(imageViewPlayer)

                textViewPlayerShortName.text = player.shortName
                textViewPlayerJumperNumber.text = player.getJumperNumber()
                textViewPlayerPosition.text = player.getPosition()
                textViewPlayerStatValue.text = player.getStatValue()
            }
        }
    }
}