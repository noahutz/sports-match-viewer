/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sportsmatchviewer.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportFragmentManager.beginTransaction()
            .replace(R.id.mainContainerLayout,
                MatchListFragment()
            )
            .commit()
    }
}
