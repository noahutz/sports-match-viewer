package com.example.sportsmatchviewer.data.repository.request

import java.net.URL

object MatchRequest {

    private const val URL_MATCH = "https://statsapi.foxsports.com.au/3.0/api/sports/league/matches/NRL20172101/topplayerstats.json;type=fantasy_points;type=tackles;type=runs;type=run_metres?limit=5&userkey=A00239D3-45F6-4A0A-810C-54A347F144C2"

    fun getData() : String {
        return URL(URL_MATCH).readText()
    }
}