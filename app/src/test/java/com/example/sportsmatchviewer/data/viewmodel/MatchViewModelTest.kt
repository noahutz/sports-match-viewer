/*
 * Copyright (c) 2019.
 * Noah Utzurrum
 */

package com.example.sportsmatchviewer.data.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.sportsmatchviewer.data.repository.MatchRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MatchViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: MatchRepository

    @InjectMocks
    lateinit var viewModel: MatchViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Test Repository invocation on ViewModel load`() {
        viewModel.loadData()
        verify(repository).getMatchList()
    }
}