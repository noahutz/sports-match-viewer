# Sports Match Viewer

### Libraries Used
1. Mockito - mock unit tests
2. Gson - text json format to class parsing 
3. Picasso - image url loading

### Project Flavors
1. mock - data is loaded from a constant text with mocked network connection delay of 5s
2. prod - data is loaded from Stats API (https://statsapi.foxsports.com.au/3.0/api/sports/league/matches/NRL20172101/topplayerstats.json;type=fantasy_points;type=tackles;type=runs;type=run_metres?limit=5&userkey=A00239D3-45F6-4A0A-810C-54A347F144C2)

![Preview Image](https://bitbucket.org/noahutz/sports-match-viewer/raw/ebcf286d4b602ee3afb5213819dd36ba1868cd04/Preview.png = 50px)